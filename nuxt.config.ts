// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  // devtools: { enabled: true },
  app:{
    buildAssetsDir: 'static', //修改站点资产的文件夹名称，默认是_nuxt
  },
  experimental: {
    payloadExtraction: false //提取使用nuxt generate生成的页面的有效负载
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData:
            "@import '~/assets/styles/theme.scss';"
        }
      }
    }
  },
  css: [
    '~/assets/styles/reset.scss',
    '~/assets/styles/common.scss',
    // '~/assets/styles/theme.scss'
  ],
})
