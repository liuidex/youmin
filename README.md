# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.


Git 全局设置:

git config --global user.name "liujie"
git config --global user.email "zm1506716201@163.com"
创建 git 仓库:

mkdir youmin
cd youmin
git init 
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/liuidex/youmin.git
git push -u origin "master"
已有仓库?

cd existing_git_repo
git remote add origin https://gitee.com/liuidex/youmin.git
git push -u origin "master"